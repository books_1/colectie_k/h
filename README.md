# H

## Content

```
./H. B. Hickey:
H. B. Hickey - Hilda 1.0 '{SF}.docx

./H. G. Konsalik:
H. G. Konsalik - Infern la Monte Cassino 0.6 '{ActiuneRazboi}.docx

./H. J. Magog:
H. J. Magog - Captivul padurii virgine 1.0 '{Tineret}.docx

./H. Laugt:
H. Laugt - Trebuie crestinul sa tina sabatul 0.9 '{Religie}.docx

./H. Rider Haggard:
H. Rider Haggard - Fiica lui Montezuma 1.0 '{Western}.docx

./H2o:
H2o - V1 Transformarea magica 1.0 '{AventuraTineret}.docx
H2o - V2 O viata plina de secrete 0.6 '{AventuraTineret}.docx
H2o - V3 Aventuri in coada de sirena 0.9 '{AventuraTineret}.docx
H2o - V4 Petrecere in pijamale 0.9 '{AventuraTineret}.docx
H2o - V5 Concursul 1.0 '{AventuraTineret}.docx
H2o - V6 Emotiile iubirii 1.0 '{AventuraTineret}.docx
H2o - V7 Sub vraja lunii 1.0 '{AventuraTineret}.docx
H2o - V8 Experimente periculoase 1.0 '{AventuraTineret}.docx

./Haiavatha:
Haiavatha - Capetenia pieilor rosii 1.0 '{AventuraTineret}.docx

./Hakan Nesser:
Hakan Nesser - Cu ochii mintii 2.0 '{Politista}.docx
Hakan Nesser - Intoarcerea 0.8 '{Politista}.docx

./Halldor Laxness:
Halldor Laxness - Clopotul din Islanda 1.0 '{Literatura}.docx

./Hallgrimur Helgason:
Hallgrimur Helgason - Femeia la 1000 grade Celsius 1.0 '{Literatura}.docx

./Hal Lindsey & C. Carison:
Hal Lindsey & C. Carison - Agonia batranei noastre planete 0.9 '{Politica}.docx

./Han Kang:
Han Kang - Disectie 1.0 '{Thriller}.docx
Han Kang - Vegetariana 1.0 '{Literatura}.docx

./Hannah Arendt:
Hannah Arendt - Conditia umana 1.0 '{Istorie}.docx
Hannah Arendt - Eichmann la Ierusalim. Raport asupra banalitatii raului 2.0 '{Istorie}.docx
Hannah Arendt - Fagaduinta politicii 1.0 '{Istorie}.docx
Hannah Arendt - Originile totalitarismului 2.0 '{Istorie}.docx

./Hannes Rastam:
Hannes Rastam - Cazul Thomas Quick 1.0 '{Thriller}.docx

./Hannes Stein:
Hannes Stein - Cum sa ai intotdeauna dreptate 0.9 '{Diverse}.docx

./Hannu Rajaniemi:
Hannu Rajaniemi - Jean le Flambeur - V1 Hotul cuantic 1.0 '{SF}.docx
Hannu Rajaniemi - Jean le Flambeur - V2 Printul fractal 1.0 '{SF}.docx
Hannu Rajaniemi - Jean le Flambeur - V3 Ingerul cauzalitatii 1.0 '{SF}.docx

./Hanny Alders:
Hanny Alders - Amurgul templierilor 1.0 '{AventuraIstorica}.docx

./Hans Christian Andersen:
Hans Christian Andersen - Cele mai frumoase povesti 1.0 '{BasmesiPovesti}.docx
Hans Christian Andersen - Povesti 1.0 '{BasmesiPovesti}.docx

./Hans Christian Huf:
Hans Christian Huf - Sfinx. Tainele Istoriei 1.0 '{MistersiStiinta}.docx

./Hans Fallada:
Hans Fallada - Si acum ce facem, micutule 0.9 '{Literatura}.docx
Hans Fallada - Singur in Berlin 1.0 '{Literatura}.docx

./Hans H. Lusthoff:
Hans H. Lusthoff - Atacati in zori 1.0 '{ActiuneRazboi}.docx

./Hans Heinrich Pars:
Hans Heinrich Pars - Viata aventuroasa a operelor de arta 0.8 '{Diverse}.docx

./Hans Heintz Ewers:
Hans Heintz Ewers - Paianjenul 1.0 '{Politista}.docx

./Hans Hermann Hoppe:
Hans Hermann Hoppe - Teoria socialismului si capitalismului 0.99 '{Politica}.docx

./Hansjorg Martin:
Hansjorg Martin - Curiozitate periculoasa 1.0 '{Tineret}.docx

./Hans Warren:
Hans Warren - Aventurile Submarinului Dox - V01 Grozaviile marilor 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V02 Razbunarea lui Satan 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V03 Lupta printului Ando 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V04 Sticla plutitoare 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V05 In ghearele primejdiei 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V06 Pe urmele corabiei negre 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V07 Printre vanatorii de capete 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V08 Capitanul Henry 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V09 Insula cu stafii 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V10 Epava misterioasa 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V11 Profesorul Barner 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V12 Castelul din mare 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V13 Taina chinezului 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V14 Jonca enigmatica 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V15 Monstrul sfant 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V16 Satul groazei 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V17 Demonul fluviului 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V18 Descoperirea doctorului Jellot 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V19 Piratii crucii 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V20 Corabia fantoma 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V21 Calaretul valurilor 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V22 Jonny din Sumatra 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V23 Rechinul urias 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V24 Idolul indian 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V25 Aventura Sanjei 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V26 Cainele credincios 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V27 Corabierul de sclavi 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V28 Capetenia Noji 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V29 Monstrii marilor 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V30 Hotul margaritarelor 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V31 Balena miraculoasa 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V32 Tainele marii 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V33 Lumi disparute 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V34 Femeia pirat 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V35 Zeul marii 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V36 Lordul Clive 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V37 Padurea in flacari 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V38 Fantoma din padure 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V39 Surorile dusmane 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V40 Uriasul Pongo 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V41 Aurul blestemat 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V42 In lupta cu chinezii 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V43 Insula vrajita 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V44 Mlastina mortii 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V45 Lupte sangeroase 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V46 Eroul Bulu 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V47 Pe insula dracului 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V48 Piratii chinezi 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V49 Durerea unui tata 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V50 Aventuri la Singapore 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V51 Elefantul alb 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V52 Preotii focului 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V53 Orasul scufundat 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V54 Sabia dreptatii 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V55 Submarinul in primejdie 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V56 Furtul zeului 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V57 Comoara bunicului 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V58 Insula tigrilor 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V59 Muntele vrajit 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V60 Ratacitorul noptii 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V61 Demonul Alompra 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V62 Printesa Sindia 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V63 Lumi plutitoare 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V64 Mumia misterioasa 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V65 Lupii din Tarai 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V66 Diavolii pacificului 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V67 Printul din Nepal 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V68 Domnita junglei 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V69 Vanatorii de balene 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V70 Marea in flacari 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V71 Dusmanul Maharadjahului 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V72 Banditii din Tibet 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V73 Printre fanatici 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V74 Corabia mortii 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V75 Punguta fermecata 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V76 Orasul demonilor 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V77 Banda lui Sao-Shung 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V78 Fudsijama, muntele sfant 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V79 Taina din Menado 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V80 Fiara rosie 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V81 Stapanul ursilor 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V82 O tragedie la fluviul Yukon 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V83 Prin pustiul de gheata 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V84 Vaporul celor pierduti 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V85 Paianjenul de mare 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V86 Intamplari misterioase 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V87 Prin subteranele din San Francisco 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V88 In Mexicul salbatec 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V89 O lupta in padurile seculare 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V90 Omul cu un singur ochi 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V91 Orasul minunilor 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V92 La fluviul amazoanelor 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V93 In tara gorilei 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V94 Capitanul Mors 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V95 Spaima Penang-ului 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V96 Pongo 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V97 Lupta pentru putere 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V98 Forte misterioase 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V99 Bricul furat 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V100 Submarinul fantoma 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V101 Un popor grozav 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V102 Prin continentul negru 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V103 Fugariti 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V104 Idolul de pe insula 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V105 Piratii Gangelui 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V106 Pe drumul caravanelor 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V107 In mare primejdie 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V108 Comoara din fildes 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V109 Guzganii gli-ului 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V110 Asociatia elefantilor 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V111 Duhurile muntilor 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V112 Piratii fluviului Sabi 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V113 Lupte cu serpi 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V114 Minuni sub pamant 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V115 Zeita de aur 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V116 Ape otravite 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V117 Diavolul 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V118 Zeul crocodililor 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V119 Banditul Jim Randle 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V120 Omul leu 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V121 Taina lui Elliot 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V122 Cainii salbatici 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V123 Tigrul bland 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V124 In Buenos Aires 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V125 Pe malurile Paraguayului 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V126 In desertul Gran Chaco 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V127 Pustnicul 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V128 Zeul din Goracpur 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V129 Indienii din America de Sud 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V130 In Bolivia 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V131 O misiune ciudata 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V132 Stafia bricului 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V133 Doctorul Nokita 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V134 In Australia 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V135 Mancatorii de oameni 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V136 Printre banditii de codru 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V137 Mamba 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V138 cu barca in jurul lumii 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V139 Orca 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V140 Tigrul verde 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V141 In tara margaritarelor 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V142 Orasul din padure 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V143 Pe o plantatie accidentata 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V144 In insula Mauriciu 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V145 Ucigasul din Madras 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V146 Razbunarea sangelui 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V147 Paianjenul galben 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V148 Tainele Madagascarului 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V149 O vanatoare de hipopotami 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V150 Fakirul 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V151 Printesa moarta 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V152 Opiu 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V153 La Bombay 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V154 Vanatorii de sclavi 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V155 Vrajitorul african 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V156 Tigrul razbunator 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V157 Printul si piticul 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V158 Sugrumatorul 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V159 Nebunul 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V160 Braul fermecat 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V161 Spaima Ugandei 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V162 Comoara profesorului 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V163 Puterea zeului 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V164 Paianjenii doctorului Galla 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V165 Pe fluviul Ruaha 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V166 La triburile africane 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V167 Soparla turbata 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V168 Demonul Gangelui 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V169 Ochiul lui Siva 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V170 Satul duhurilor 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V171 Prizonieri in Madagascar 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V172 Vraja din Marea Sargaselor 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V173 Un al doilea Farrow 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V174 Dansatoarea din templu 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V175 Cavalcada fantomelor 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V176 Corabia ocnasilor 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V177 Pelota, omul din tara de foc 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V178 In orasul mort 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V179 Fantoma marii 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V180 Cea mai grea lupta a lui Pongo 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V181 Taine stravechi 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V182 Capetenia daiaca 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V183 Printre indieni 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V184 Generalul Gonzalo 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V185 Vanatoare asupra submarinului 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V186 Bufonul broastei testoase 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V187 Capetenia Tulopi 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V188 Mesagerul mortii 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V189 Lumina tremuratoare 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V190 Ruina misterioasa 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V191 Nepotul lui Mister Tomkins 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V192 Breasla cersetorilor 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V193 Vanatorii de oameni 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V194 Piratii congolezi 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V195 Steaua de mare 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V196 Aventura la paraul pestelui 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V197 Moarte si inviere in Hawaii 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V198 Glontul de otel 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V199 In tara lui Kubu 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V200 Regele junglei 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V201 Sfera de otel 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V202 Primejdiile Amazonului 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V203 Om peste bord! 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V204 Omul din Padang 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V205 Blestemul lui Ali Ben Murpa 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V206 Misterul din Nikobare 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V207 Sageata otravita 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V208 O vanatoare nereusita 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V209 Inotatorul misterios 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V210 Capitanul Farrow 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V211 Elefantul sacru 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V212 Stapanul muntilor 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V213 Capetenia falsa 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V214 Piticii periculosi 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V215 Demonul din Purry 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V216 Spaima Sudarbans-ului 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V217 Gorila de munte 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V218 Marian Farrow salvatorul 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V219 Spaima din Loango 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V220 Banditul Fernando 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V221 Zeul crocodililor 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V222 In ghearele vanatorilor de capete 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V223 Kuma din Asanti 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V224 Lancierul 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V225 Preotul Bhunwa 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V226 Liga panterelor 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V227 Bady, leoaica din Senegal 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V228 Seicul Houd 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V229 Diavolul din Casmir 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V230 Nepalul, tara miracolelor 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V231 Maura Tiris 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V232 La lacul Wular 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V233 Poveste indiana 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V234 Corabia piratilor 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V235 Pantera neagra 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V236 Rechinul Marii Sudului 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V237 Tam tam mortal in Congo 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V238 Grota diavolului rosu 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V239 Regele Mangaia 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V240 In primejdie de moarte 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V241 Ochi pentru ochi 1.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V242 Evadare din iad 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V243 Osanditi la moarte 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V244 Ocnasi in Numea 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V245 Caravana mortii 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V246 Focul sacru 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V247 Diavolul din Tarai 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V248 Expeditia doctorului Monti 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V249 In tara lui Pongo 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V250 Comoara de pe Selawa 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V251 Zile de groaza la Guanahani 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V252 In craterul de la Martinica 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V253 Razbunatorul sinistru 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V254 Java Jim 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V255 Comoara piratului 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V256 Pasagerul clandestin 2.0 '{Tineret}.docx
Hans Warren - Aventurile Submarinului Dox - V257 Un secret inspaimantator 2.0 '{Tineret}.docx

./Hans Werner Richter:
Hans Werner Richter - Sa nu ucizi 1.0 '{Tineret}.docx

./Hanya Yanagihara:
Hanya Yanagihara - O viata marunta 1.0 '{Tineret}.docx

./Haralambie Barzan:
Haralambie Barzan - Partea nevazuta a zilei de maine 1.0 '{Politista}.docx

./Haralamb Zinca:
Haralamb Zinca - Aventurierul 0.9 '{Politista}.docx
Haralamb Zinca - B39 - V1 Soarele a murit in zori 4.0 '{Politista}.docx
Haralamb Zinca - B39 - V2 Toamna cu frunze negre 4.0 '{Politista}.docx
Haralamb Zinca - B39 - V3 Anotimpurile mortii 1.1 '{Politista}.docx
Haralamb Zinca - B39 - V4 Destinul capitanului Iamandi 2.0 '{Politista}.docx
Haralamb Zinca - B39 - V5 Operatiunea Soare 2.0 '{Politista}.docx
Haralamb Zinca - Ceasurile sfantului Bartolomeu 2.0 '{Politista}.docx
Haralamb Zinca - Crima de la 217 1.0 '{Politista}.docx
Haralamb Zinca - Disparut fara urma 1.0 '{Politista}.docx
Haralamb Zinca - Dosarul aviatorului singuratic 2.0 '{Politista}.docx
Haralamb Zinca - Dragul meu Sherlock Holmes 1.0 '{Politista}.docx
Haralamb Zinca - Eu, H.Z., aventurierul 1.0 '{Politista}.docx
Haralamb Zinca - Interpolul transmite arestati-l! 1.0 '{Politista}.docx
Haralamb Zinca - Jurnal de front 1.0 '{Politista}.docx
Haralamb Zinca - Limuzina neagra 1.0 '{Politista}.docx
Haralamb Zinca - Moartea m-a batut pe umar 1.0 '{Politista}.docx
Haralamb Zinca - Moartea vine pe banda de magnetofon 1.0 '{Politista}.docx
Haralamb Zinca - Ochii doctorului King 2.0 '{Politista}.docx
Haralamb Zinca - O crima aproape perfecta 1.0 '{Politista}.docx
Haralamb Zinca - Revelion 45 1.0 '{Politista}.docx
Haralamb Zinca - Sfarsitul spionului fantoma 1.0 '{Politista}.docx
Haralamb Zinca - Si a fost ora H 1.0 '{Politista}.docx
Haralamb Zinca - Supersonicul 01 decoleaza in zori 1.0 '{Politista}.docx
Haralamb Zinca - Taina cavalerului de Dolenga 1.0 '{Politista}.docx
Haralamb Zinca - Ultima noapte de razboi, prima zi de pace 2.0 '{Politista}.docx
Haralamb Zinca - Un caz de disparitie 1.0 '{Politista}.docx
Haralamb Zinca - Un glonte pentru rezident 2.0 '{Politista}.docx

./Haralan Popov:
Haralan Popov - Torturat pentru credinta 0.9 '{Religie}.docx

./Hari Das Melchizedek:
Hari Das Melchizedek - Meditatie pentru activarea ADN 0.9 '{Spiritualitate}.docx

./Harlan Coben:
Harlan Coben - Destine furate 1.0 '{Thriller}.docx
Harlan Coben - Disparut fara urma 1.0 '{Thriller}.docx
Harlan Coben - Padurea 1.0 '{Thriller}.docx
Harlan Coben - Sase ani 1.0 '{Thriller}.docx
Harlan Coben - Un mesaj socant 1.0 '{Thriller}.docx

./Harlan Ellison:
Harlan Ellison - Caieste-te Arlechin 1.1 '{SF}.docx
Harlan Ellison - Pasarea mortii 1.0 '{SF}.docx
Harlan Ellison - Un baiat si cainele sau 2.0 '{SF}.docx

./Harper Lee:
Harper Lee - Sa ucizi o pasare cantatoare 0.99 '{Literatura}.docx

./Harriet Lummis Smith:
Harriet Lummis Smith - Secretul multumirii V1 0.9 '{SF}.docx
Harriet Lummis Smith - Secretul multumirii V2 1.0 '{SF}.docx
Harriet Lummis Smith - Secretul multumirii V3 1.0 '{SF}.docx

./Harry A. Gailey:
Harry A. Gailey - Victoria imposibilului 1.0 '{Razboi}.docx

./Harry Anderson:
Harry Anderson - Pe urmele lui Livingstone 0.6 '{Diverse}.docx

./Harry Mulisch:
Harry Mulisch - Siegfried - O idila neagra 0.7 '{Razboi}.docx

./Hartwig Hausdorf:
Hartwig Hausdorf - Experimentul Pamant 0.9 '{MistersiStiinta}.docx

./Haruki Murakami:
Haruki Murakami - 1Q84 V1 3.0 '{Aventura}.docx
Haruki Murakami - 1Q84 V2 3.0 '{Aventura}.docx
Haruki Murakami - 1Q84 V3 3.0 '{Aventura}.docx
Haruki Murakami - Autoportretul scriitorului ca alergator de cursa lunga 0.7 '{Aventura}.docx
Haruki Murakami - Barbati fara femei 1.0 '{Aventura}.docx
Haruki Murakami - Cronica pasarii Arc 1.0 '{Aventura}.docx
Haruki Murakami - Dans, dans, dans 1.1 '{Aventura}.docx
Haruki Murakami - Dupa cutremur 1.0 '{Aventura}.docx
Haruki Murakami - Elefantul a disparut 1.0 '{Aventura}.docx
Haruki Murakami - In cautarea oii fantastice 1.1 '{Aventura}.docx
Haruki Murakami - In noapte 1.0 '{Aventura}.docx
Haruki Murakami - Iubita mea Sputnik 2.0 '{Aventura}.docx
Haruki Murakami - Kafka pe malul marii 1.0 '{Aventura}.docx
Haruki Murakami - La sud de granita, la vest de soare 1.1 '{Aventura}.docx
Haruki Murakami - Meseria de romancier 1.0 '{Aventura}.docx
Haruki Murakami - Padurea norvegiana 3.0 '{Aventura}.docx
Haruki Murakami - Salcia oarba, fata adormita 0.6 '{Aventura}.docx
Haruki Murakami - The Rat - V1-2 Asculta cum canta vantul. Pinball 1.0 '{Aventura}.docx
Haruki Murakami - Tsukuru Tazaki cel fara de culoare si anii sai de pelerinaj 1.0 '{Aventura}.docx
Haruki Murakami - Uciderea comandorului - V1 O idee isi face aparitia 2.0 '{Aventura}.docx
Haruki Murakami - Uciderea Comandorului - V2 Metafora se schimba 1.0 '{Aventura}.docx

./Harvey Dixon:
Harvey Dixon - La atac 1.0 '{ActiuneRazboi}.docx

./Haylen Beck:
Haylen Beck - Disparitia din Silver Water 1.0 '{Thriller}.docx

./Heather Davis:
Heather Davis - V1 Nu plange sub clar de luna 1.0 '{Vampiri}.docx
Heather Davis - V2 Uneori, sub clar de luna 1.0 '{Vampiri}.docx

./Heather Miller:
Heather Miller - Stanca pescarusilor 0.9 '{Dragoste}.docx

./Heather Morris:
Heather Morris - Tatuatorul de la Auschwitz 1.0 '{Razboi}.docx

./Heather Skyler:
Heather Skyler - Varsta perfecta 1.0 '{Literatura}.docx

./Heather Tucker:
Heather Tucker - Fata din lut 1.0 '{Literatura}.docx

./Hector Malot:
Hector Malot - Singur pe lume 2.0 '{Tineret}.docx

./Heikki Toppila:
Heikki Toppila - Fiul mortii 0.99 '{Horror}.docx

./Heinrich Boll:
Heinrich Boll - Casa vaduvelor 0.7 '{Literatura}.docx

./Heinrich Harrer:
Heinrich Harrer - 7 ani in Tibet 1.0 '{Literatura}.docx
Heinrich Harrer - Intoarcerea in Tibet 1.0 '{Literatura}.docx

./Heinrich Mann:
Heinrich Mann - Henri al IV-lea - V1 Tineretea lui Henri al IV-lea 1.0 '{AventuraIstorica}.docx
Heinrich Mann - Henri al IV-lea - V2 Implinirea si sfarsitul lui Henri al IV-lea 1.0 '{AventuraIstorica}.docx
Heinrich Mann - Ingerul albastru 2.0 '{Dragoste}.docx

./Helen Carter:
Helen Carter - Lacrimi de aur 0.9 '{Romance}.docx
Helen Carter - O viata impreuna cu tine 0.9 '{Romance}.docx

./Helen Conrad:
Helen Conrad - Calcaiul lui Ahile 0.9 '{Dragoste}.docx
Helen Conrad - Farmecul tropicelor 0.9 '{Dragoste}.docx
Helen Conrad - S-au cunoscut in Zanzibar 0.9 '{Romance}.docx
Helen Conrad - Talismanul cu delfini 0.9 '{Dragoste}.docx

./Helene Gremillon:
Helene Gremillon - Confidentul 1.0 '{Literatura}.docx
Helene Gremillon - Tangou pentru Lisandra 1.0 '{Literatura}.docx

./Helene Grimaud:
Helene Grimaud - Lectii aparte 1.0 '{Literatura}.docx

./Helene Pasquier:
Helene Pasquier - Dubla lovitura 1.0 '{Thriller}.docx

./Helen Fielding:
Helen Fielding - Bridget Jones - V1 Jurnalul lui Bridget Jones 0.9 '{Dragoste}.docx
Helen Fielding - Bridget Jones - V2 La limita ratiunii 0.99 '{Dragoste}.docx
Helen Fielding - Olivia Joules si imaginatia hiperactiva 0.9 '{Dragoste}.docx

./Helen Mittermeyer:
Helen Mittermeyer - Destiny Smith 0.9 '{Dragoste}.docx

./Helen Ray:
Helen Ray - Agentia de modele 0.7 '{Dragoste}.docx
Helen Ray - Regina gheturilor 0.9 '{Dragoste}.docx
Helen Ray - Sarutul interzis 0.9 '{Romance}.docx
Helen Ray - Sunt timida 0.9 '{Dragoste}.docx

./Helen Star:
Helen Star - Un loc in inima ta 0.9 '{Romance}.docx

./Helmut Sakowski:
Helmut Sakowski - Daniel Druskat 0.6 '{Literatura}.docx

./Hendrik Groen:
Hendrik Groen - Jurnalul secret al lui Hendrik Groen 1.0 '{Jurnal}.docx

./Henning Mankell:
Henning Mankell - Pantofi italienesti 1.0 '{Literatura}.docx
Henning Mankell - Ucigasi fara chip 1.0 '{Thriller}.docx

./Henri Bergson:
Henri Bergson - Cele doua surse ale moralei 0.9 '{Spiritualitate}.docx
Henri Bergson - Evolutia creatoare 0.6 '{Spiritualitate}.docx
Henri Bergson - Gandirea si miscarea 0.8 '{Spiritualitate}.docx
Henri Bergson - Materie si memorie 0.7 '{Spiritualitate}.docx

./Henri Bernard & Roger Gheysens:
Henri Bernard & Roger Gheysens - Batalia din Ardeni 1.0 '{Razboi}.docx

./Henri Charriere:
Henri Charriere - Banco 2.0 '{Aventura}.docx
Henri Charriere - Papillon 4.0 '{Aventura}.docx

./Henri de Regnier:
Henri de Regnier - Sarpele cu doua capete 1.0 '{Dragoste}.docx
Henri de Regnier - Vapaia 1.0 '{Dragoste}.docx

./Henriette Beecher Stowe:
Henriette Beecher Stowe - Coliba unchiului Tom 1.0 '{Tineret}.docx

./Henriette Yvonne Stahl:
Henriette Yvonne Stahl - Drum de foc 1.0 '{Literatura}.docx
Henriette Yvonne Stahl - Fratele meu omul 1.0 '{Literatura}.docx
Henriette Yvonne Stahl - Intre zi si noapte 1.0 '{Literatura}.docx
Henriette Yvonne Stahl - Lena, fata lui Anghel Margarit 1.0 '{Literatura}.docx
Henriette Yvonne Stahl - Marea bucurie 1.0 '{Literatura}.docx
Henriette Yvonne Stahl - Martorul eternitatii 1.0 '{Literatura}.docx
Henriette Yvonne Stahl - Matusa Matilda 1.0 '{Literatura}.docx
Henriette Yvonne Stahl - Nu ma calca pe umbra 1.0 '{Literatura}.docx
Henriette Yvonne Stahl - Pontiful 2.0 '{Literatura}.docx
Henriette Yvonne Stahl - Realitatea iluziei 0.8 '{Literatura}.docx
Henriette Yvonne Stahl - Steaua robilor 1.0 '{Literatura}.docx
Henriette Yvonne Stahl - Voica 1.0 '{Literatura}.docx

./Henri James:
Henri James - Piata Washington 0.6 '{Literatura}.docx
Henri James - Portretul unei doamne 0.6 '{Literatura}.docx

./Henrik Fexeus:
Henrik Fexeus - Arta de a citi gandurile 0.99 '{Psihologie}.docx

./Henrik Ibsen:
Henrik Ibsen - Constructorul Solness 0.99 '{Teatru}.docx
Henrik Ibsen - Femeia marii 0.8 '{Teatru}.docx
Henrik Ibsen - Hedda Gabler 1.0 '{Teatru}.docx
Henrik Ibsen - O casa de papusi 0.9 '{Teatru}.docx
Henrik Ibsen - Rosmersholm 1.0 '{Teatru}.docx
Henrik Ibsen - Stalpii societatii 1.0 '{Teatru}.docx
Henrik Ibsen - Teatru V1 1.0 '{Teatru}.docx
Henrik Ibsen - Teatru V2 1.0 '{Teatru}.docx
Henrik Ibsen - Teatru V3 1.0 '{Teatru}.docx

./Henri Landemer:
Henri Landemer - Waffen SS 1.0 '{Razboi}.docx

./Henri Queffelec:
Henri Queffelec - O lumina se aprinde pe mare 1.0 '{Dragoste}.docx

./Henri Stahl:
Henri Stahl - Un roman in luna 1.0 '{SF}.docx

./Henry Castillou:
Henry Castillou - Furtuna din iulie 2.0 '{Dragoste}.docx

./Henry de Montherlant:
Henry de Montherlant - Baietii 0.9 '{Literatura}.docx
Henry de Montherlant - Bestiarele 0.9 '{Literatura}.docx
Henry de Montherlant - Cazul Exupere 0.9 '{Literatura}.docx
Henry de Montherlant - Demonul binelui 0.9 '{Literatura}.docx
Henry de Montherlant - Indurare pentru femei 0.9 '{Literatura}.docx
Henry de Montherlant - Leproasele 0.9 '{Literatura}.docx
Henry de Montherlant - Malatesta 0.7 '{Literatura}.docx
Henry de Montherlant - Micuta infanta de Castilia 0.9 '{Literatura}.docx
Henry de Montherlant - Vinul 0.9 '{Literatura}.docx

./Henry Fielding:
Henry Fielding - Joseph Andrews 0.7 '{Literatura}.docx

./Henry Gregor Felsen:
Henry Gregor Felsen - Pe planeta Adnaxas nu sunt copaci 1.0 '{SF}.docx

./Henry Jaeger:
Henry Jaeger - Disperatii 1.0 '{Literatura}.docx

./Henry James:
Henry James - Intre doua tarmuri 2.0 '{Dragoste}.docx
Henry James - Piata Washington 1.0 '{Dragoste}.docx
Henry James - Portretul unei doamne 0.9 '{Dragoste}.docx

./Henry Kissinger:
Henry Kissinger - Diplomatia 1.0 '{Istorie}.docx

./Henryk Sienkiewicz:
Henryk Sienkiewicz - Aniela 1.0 '{ClasicSt}.docx
Henryk Sienkiewicz - Cavalerii teutoni 4.0 '{ClasicSt}.docx
Henryk Sienkiewicz - Familia Polaniecky V1 1.0 '{ClasicSt}.docx
Henryk Sienkiewicz - Familia Polaniecky V2 1.0 '{ClasicSt}.docx
Henryk Sienkiewicz - Familia Polaniecky V3 1.0 '{ClasicSt}.docx
Henryk Sienkiewicz - Hania 1.0 '{ClasicSt}.docx
Henryk Sienkiewicz - In tara aurului 2.0 '{ClasicSt}.docx
Henryk Sienkiewicz - Paznicul farului 1.0 '{ClasicSt}.docx
Henryk Sienkiewicz - Pe camp de glorie 2.0 '{ClasicSt}.docx
Henryk Sienkiewicz - Quo Vadis 4.0 '{ClasicSt}.docx
Henryk Sienkiewicz - V1 Prin foc si sabie 2.0 '{ClasicSt}.docx
Henryk Sienkiewicz - V2 Potopul V1 2.0 '{ClasicSt}.docx
Henryk Sienkiewicz - V2 Potopul V2 2.0 '{ClasicSt}.docx
Henryk Sienkiewicz - V2 Potopul V3 2.0 '{ClasicSt}.docx
Henryk Sienkiewicz - V3 Pan Wolodyjowski 2.0 '{ClasicSt}.docx

./Henry Lawson:
Henry Lawson - Povestiri din Australia 1.0 '{Literatura}.docx
Henry Lawson - Rivalul lui Andy Page 1.0 '{Literatura}.docx

./Henry Miller:
Henry Miller - Cosmarul climatizat 0.9 '{Literatura}.docx
Henry Miller - Opus Pistorum 1.0 '{Porno}.docx
Henry Miller - Primavara neagra 0.9 '{Literatura}.docx
Henry Miller - Rastignire Trandafirie - V1 Sexus 0.9 '{Literatura}.docx
Henry Miller - Rastignire Trandafirie - V2 Plexus 0.8 '{Literatura}.docx
Henry Miller - Rastignire Trandafirie - V3 Nexus 0.99 '{Literatura}.docx
Henry Miller - Tropicul capricornului 0.9 '{Literatura}.docx
Henry Miller - Zile linistite la Clichy 0.9 '{Literatura}.docx

./Henry Sleasar:
Henry Sleasar - Buna dimineata 1.0 '{SF}.docx

./Henry Wade:
Henry Wade - Vanatorul de lorzi 1.0 '{Politista}.docx

./Herbert Genzmer:
Herbert Genzmer - Dali & Gala 1.0 '{Literatura}.docx

./Herbert Krosney:
Herbert Krosney - Evanghelia pierduta 1.0 '{MistersiStiinta}.docx

./Herbert W. Franke:
Herbert W. Franke - Capcana de sticla 1.0 '{SF}.docx
Herbert W. Franke - Expeditia 1.0 '{SF}.docx
Herbert W. Franke - Neincredere 1.0 '{SF}.docx
Herbert W. Franke - Reteaua gandurilor 1.1 '{SF}.docx
Herbert W. Franke - Visul lui despre mare 0.99 '{SF}.docx
Herbert W. Franke - Vrem sa-l vedem pe Darius Miller 1.0 '{SF}.docx
Herbert W. Franke - Zona zero 1.0 '{SF}.docx

./Herman Bang:
Herman Bang - Tine. La calea ferata 1.0 '{Dragoste}.docx

./Herman Melville:
Herman Melville - Cojocelul alb V1 1.0 '{Aventura}.docx
Herman Melville - Cojocelul alb V2 0.7 '{Aventura}.docx
Herman Melville - Moby Dick 1.0 '{Aventura}.docx

./Hermann Broch:
Hermann Broch - Moartea lui Virgiliu 0.9 '{Literatura}.docx
Hermann Broch - Vrajirea 0.9 '{Literatura}.docx

./Hermann Hesse:
Hermann Hesse - Calatoria spre Soare Rasare 0.9 '{ClasicSt}.docx
Hermann Hesse - Gertrud. Rosshalde. Ultima vara a lui Klingsor 1.0 '{ClasicSt}.docx
Hermann Hesse - Jocul cu margele de sticla 1.0 '{ClasicSt}.docx
Hermann Hesse - Lupul de stepa 1.0 '{ClasicSt}.docx
Hermann Hesse - Narcis si gura de aur 1.0 '{ClasicSt}.docx
Hermann Hesse - Siddhartha 1.0 '{ClasicSt}.docx

./Hermann von Keyserling:
Hermann von Keyserling - Viata intima 0.8 '{Eseuri}.docx

./Herman Wouk:
Herman Wouk - Revolta de pe Caine 1.0 '{ActiuneRazboi}.docx

./Hermina Black:
Hermina Black - Dragostea de odinioara 0.99 '{Romance}.docx
Hermina Black - Farsa periculoasa 0.9 '{Dragoste}.docx
Hermina Black - Fiica Fortunei 0.9 '{Romance}.docx
Hermina Black - Sora medicala 0.9 '{Dragoste}.docx

./Herta Muller:
Herta Muller - Animalul inimii 1.0 '{Literatura}.docx
Herta Muller - Inca de pe atunci vulpea era vanatorul 0.9 '{Literatura}.docx
Herta Muller - Leaganul respiratiei 0.7 '{Literatura}.docx
Herta Muller - Omul este un mare fazan pe lume 1.0 '{Literatura}.docx
Herta Muller - Regele se inclina si ucide 0.7 '{Literatura}.docx

./Herve de Peslouan:
Herve de Peslouan - Amiralul nisipurilor 1.0 '{Tineret}.docx

./Hichens Robert Smythe:
Hichens Robert Smythe - Dragostea cu sila a profesorului Guildea 0.9 '{Literatura}.docx

./Hierotheos Vlachos:
Hierotheos Vlachos - Boala si tamaduirea sufletului in traditia ortodoxa 1.0 '{Religie}.docx

./Hikaru Okuizumi:
Hikaru Okuizumi - Strigatul pietrelor 0.9 '{Diverse}.docx

./Hilary Wilde:
Hilary Wilde - Amantii furtunii 0.99 '{Romance}.docx
Hilary Wilde - Marea de turcoaz 0.99 '{Dragoste}.docx
Hilary Wilde - Promisiunea unei vieti fericite 0.99 '{Romance}.docx

./Hillary Neal:
Hillary Neal - Sub acelasi acoperis 0.99 '{Dragoste}.docx

./Hiro Arikawa:
Hiro Arikawa - Memoriile unui motan calator 1.0 '{Literatura}.docx

./Hiromi Kawakami:
Hiromi Kawakami - Cele zece iubiri ale lui Nishino 1.0 '{Literatura}.docx
Hiromi Kawakami - Pravalia de maruntisuri a domnului Nakano 1.0 '{Literatura}.docx

./Hjalmar Soderberg:
Hjalmar Soderberg - Jocul serios 0.9 '{Literatura}.docx

./Holly Black:
Holly Black - Basme Moderne - V1 Jertfa 1.9 '{BasmesiPovesti}.docx
Holly Black - Basme Moderne - V2 Curajul 1.9 '{BasmesiPovesti}.docx
Holly Black - Basme Moderne - V3 Tinutul fierului 1.9 '{BasmesiPovesti}.docx
Holly Black - Fapturile vazduhului - V1 Printul nemilos 0.9 '{BasmesiPovesti}.docx
Holly Black - Fapturile vazduhului - V2 Regele malefic 2.0 '{BasmesiPovesti}.docx

./Holly Black & Cassandra Clare:
Holly Black & Cassandra Clare - Magisterium - V1 Testul fierului 1.0 '{Supranatural}.docx
Holly Black & Cassandra Clare - Magisterium - V2 Manusa de arama 1.0 '{Supranatural}.docx
Holly Black & Cassandra Clare - Magisterium - V3 Cheia de bronz 1.0 '{Supranatural}.docx
Holly Black & Cassandra Clare - Magisterium - V4 Masca de argint 1.0 '{Supranatural}.docx

./Homer:
Homer - Iliada 0.9 '{AventuraIstorica}.docx

./Honore de Balzac:
Honore de Balzac - Amorul mascat 1.0 '{ClasicSt}.docx
Honore de Balzac - Comedia umana - Alt studiu de femeie 1.0 '{ClasicSt}.docx
Honore de Balzac - Comedia umana - Jean Louis 1.0 '{ClasicSt}.docx
Honore de Balzac - Medicul de tara 1.0 '{ClasicSt}.docx
Honore de Balzac - Neamul Marana 1.0 '{ClasicSt}.docx
Honore de Balzac - Scene din viata de provincie - Eugenie Grandet 1.0 '{ClasicSt}.docx
Honore de Balzac - Scene din viata de provincie - Faimosul Gaudissart 1.0 '{ClasicSt}.docx
Honore de Balzac - Scene din viata de provincie - Iluzii pierdute 1.0 '{ClasicSt}.docx
Honore de Balzac - Scene din viata de provincie - Pierrette 1.0 '{ClasicSt}.docx
Honore de Balzac - Scene din viata de provincie - Rivalitati - V1 Fata batrana 1.0 '{ClasicSt}.docx
Honore de Balzac - Scene din viata de provincie - Rivalitati - V2 Salonul cu vechituri 1.0 '{ClasicSt}.docx
Honore de Balzac - Scene din viata de provincie - Ursule Mirouet 1.0 '{ClasicSt}.docx
Honore de Balzac - Scene din viata de provincie - Vicarul din Tours 1.0 '{ClasicSt}.docx
Honore de Balzac - Scene din viata militara - Suanii 1.0 '{ClasicSt}.docx
Honore de Balzac - Scene din viata pariziana - Banca Nucingen 1.0 '{ClasicSt}.docx
Honore de Balzac - Scene din viata pariziana - Colonelul Chabert 1.0 '{ClasicSt}.docx
Honore de Balzac - Scene din viata pariziana - Comedianti fara s-o stie 1.0 '{ClasicSt}.docx
Honore de Balzac - Scene din viata pariziana - Facino Cane 1.0 '{ClasicSt}.docx
Honore de Balzac - Scene din viata pariziana - Gaudissart al II-lea 1.0 '{ClasicSt}.docx
Honore de Balzac - Scene din viata pariziana - Istoria celor treisprezece 1.0 '{ClasicSt}.docx
Honore de Balzac - Scene din viata pariziana - Istoria maririi si decaderii lui Cesar Birotteau 1.0 '{ClasicSt}.docx
Honore de Balzac - Scene din viata pariziana - Mos Goriot 1.0 '{ClasicSt}.docx
Honore de Balzac - Scene din viata pariziana - Pierre Grassou 1.0 '{ClasicSt}.docx
Honore de Balzac - Scene din viata pariziana - Punerea sub interdictie 1.0 '{ClasicSt}.docx
Honore de Balzac - Scene din viata pariziana - Sarrasine 1.0 '{ClasicSt}.docx
Honore de Balzac - Scene din viata pariziana - Secretele printesei de Cadignan 1.0 '{ClasicSt}.docx
Honore de Balzac - Scene din viata pariziana - Slujbasii 1.0 '{ClasicSt}.docx
Honore de Balzac - Scene din viata pariziana - Stralucirea si suferintele curtezanelor 1.0 '{ClasicSt}.docx
Honore de Balzac - Scene din viata pariziana - Un om de afaceri 1.0 '{ClasicSt}.docx
Honore de Balzac - Scene din viata pariziana - Varul Pons 1.0 '{ClasicSt}.docx
Honore de Balzac - Scene din viata pariziana - Verisoara Bette 1.0 '{ClasicSt}.docx
Honore de Balzac - Scene din viata politica - Z. Marcas 1.0 '{ClasicSt}.docx
Honore de Balzac - Scene din viata privata - A doua familie 1.0 '{ClasicSt}.docx
Honore de Balzac - Scene din viata privata - Bal la Sceaux 1.0 '{ClasicSt}.docx
Honore de Balzac - Scene din viata privata - Beatrix 2.0 '{ClasicSt}.docx
Honore de Balzac - Scene din viata privata - Camatarul Gobseck 1.0 '{ClasicSt}.docx
Honore de Balzac - Scene din viata privata - Casa de la Grande Breteche 1.0 '{ClasicSt}.docx
Honore de Balzac - Scene din viata privata - Casa la motanul cu mingea 1.0 '{ClasicSt}.docx
Honore de Balzac - Scene din viata privata - Contractul de casatorie 1.0 '{ClasicSt}.docx
Honore de Balzac - Scene din viata privata - Femeia de 30 ani 1.0 '{ClasicSt}.docx
Honore de Balzac - Scene din viata privata - Femeia parasita 1.0 '{ClasicSt}.docx
Honore de Balzac - Scene din viata privata - Liturghia unui ateu 1.0 '{ClasicSt}.docx
Honore de Balzac - Scene din viata privata - Memoriile a doua tinere casatorite 1.0 '{ClasicSt}.docx
Honore de Balzac - Scene din viata privata - Modeste Mignon 1.0 '{ClasicSt}.docx
Honore de Balzac - Scene din viata privata - Un debut in viata 1.0 '{ClasicSt}.docx
Honore de Balzac - Scene din viata privata - Vendetta 1.0 '{ClasicSt}.docx
Honore de Balzac - Studii filozofice - Adio 1.0 '{ClasicSt}.docx
Honore de Balzac - Studii filozofice - Capodopera necunoscuta 1.0 '{ClasicSt}.docx
Honore de Balzac - Studii filozofice - Catherine de Medicis 1.0 '{ClasicSt}.docx
Honore de Balzac - Studii filozofice - Cautarea absolutului 1.0 '{ClasicSt}.docx
Honore de Balzac - Studii filozofice - Copilul renegat 1.0 '{ClasicSt}.docx
Honore de Balzac - Studii filozofice - Elixirul de viata lunga 1.0 '{ClasicSt}.docx
Honore de Balzac - Studii filozofice - Massimilla Doni 1.0 '{ClasicSt}.docx
Honore de Balzac - Studii filozofice - Melmoth impacat 1.0 '{ClasicSt}.docx
Honore de Balzac - Studii filozofice - Pielea de sagri 1.0 '{ClasicSt}.docx
Honore de Balzac - Teatru 1.0 '{ClasicSt}.docx

./Horace McCoy:
Horace McCoy - Si caii se impusca, nu-i asa 1.0 '{Literatura}.docx

./Horia Arama:
Horia Arama - Cosmonautul cel trist 1.0 '{SF}.docx
Horia Arama - Dincolo de paradis 1.0 '{SF}.docx
Horia Arama - Moartea pasarii Sageata 1.0 '{SF}.docx
Horia Arama - Nu in fata oglinzii 0.9 '{SF}.docx
Horia Arama - Tarmul interzis 1.0 '{SF}.docx
Horia Arama - Verde Aixa 1.0 '{SF}.docx

./Horia Dulvac:
Horia Dulvac - Ingrijorari fara mandat 0.99 '{Spiritualitate}.docx

./Horia Garbea:
Horia Garbea - Rata cu portocale 1.0 '{ProzaScurta}.docx

./Horia Matei:
Horia Matei - Literatura si fascinatia aventurii 1.0 '{Diverse}.docx
Horia Matei - Maiasii 1.0 '{Civilizatii}.docx
Horia Matei - Pestera Novacului 0.9 '{SF}.docx
Horia Matei - Pirati si Corsari 1.0 '{Istorie}.docx
Horia Matei - Turneul de primavara 0.99 '{AventuraTineret}.docx

./Horia Roman Patapievici:
Horia Roman Patapievici - Cerul vazut prin lentila 0.8 '{Filozofie}.docx
Horia Roman Patapievici - Doctorul inspectat de copii 0.99 '{Filozofie}.docx
Horia Roman Patapievici - Geniul adolescentei 0.99 '{Filozofie}.docx
Horia Roman Patapievici - Omul recent 0.9 '{Filozofie}.docx
Horia Roman Patapievici - Radu Daescu 0.9 '{Filozofie}.docx

./Horia Sima:
Horia Sima - Istoria miscarii legionare 0.9 '{Politica}.docx

./Horia Stancu:
Horia Stancu - Asklepios 1.0 '{AventuraIstorica}.docx
Horia Stancu - Fanar 1.0 '{IstoricaRo}.docx

./Horia Tecuceanu:
Horia Tecuceanu - Afacerea Samoilescu Cozmici 1.0 '{Politista}.docx
Horia Tecuceanu - Capitanul Apostolescu ancheteaza. O afacere incalcita 1.0 '{Politista}.docx
Horia Tecuceanu - Capitanul Apostolescu intervine 1.0 '{Politista}.docx
Horia Tecuceanu - Capitanul Apostolescu si dubla enigma 1.0 '{Politista}.docx
Horia Tecuceanu - Capitanul Apostolescu si filiera 2.0 '{Politista}.docx
Horia Tecuceanu - Capitanul Apostolescu si fratii 1.0 '{Politista}.docx
Horia Tecuceanu - Capitanul Apostolescu si identificarea 1.0 '{Politista}.docx
Horia Tecuceanu - Capitanul Apostolescu si inamicul public nr.1 1.0 '{Politista}.docx
Horia Tecuceanu - Capitanul Apostolescu si mobilul 1.0 '{Politista}.docx
Horia Tecuceanu - Capitanul Apostolescu si piromanul 1.0 '{Politista}.docx
Horia Tecuceanu - Capitanul Apostolescu si spionii 2.0 '{Politista}.docx
Horia Tecuceanu - Obstinatia capitanului Apostolescu 1.0 '{Politista}.docx
Horia Tecuceanu -Surprizele capitanului Apostolescu 1.0 '{Politista}.docx

./Hortensia Papadat Bengescu:
Hortensia Papadat Bengescu - Concert de muzica de Bach 1.0 '{ClasicRo}.docx
Hortensia Papadat Bengescu - Fecioarele despletite 1.0 '{ClasicRo}.docx

./Hoshi Shinichi:
Hoshi Shinichi - Implicatia iubirii 0.99 '{SF}.docx

./Howard Hendrix:
Howard Hendrix - Cheia labirintului 1.0 '{AventuraIstorica}.docx

./Howard Philips Lovecraft:
Howard Philips Lovecraft - Aer rece 1.0 '{Horror}.docx
Howard Philips Lovecraft - Alchimistul 1.0 '{Horror}.docx
Howard Philips Lovecraft - Azathoth 1.0 '{Horror}.docx
Howard Philips Lovecraft - Blestemul din Sarnath 1.0 '{Horror}.docx
Howard Philips Lovecraft - Cabana din ceata 1.0 '{Horror}.docx
Howard Philips Lovecraft - Cartea 1.0 '{Horror}.docx
Howard Philips Lovecraft - Cautarea lui Iranon 2.0 '{Horror}.docx
Howard Philips Lovecraft - Cavoul 1.0 '{Horror}.docx
Howard Philips Lovecraft - Cazul Charles Dexter Ward 1.0 '{Horror}.docx
Howard Philips Lovecraft - Cel care susotea in intuneric 1.0 '{Horror}.docx
Howard Philips Lovecraft - Cel ce bantuia in bezne 1.0 '{Horror}.docx
Howard Philips Lovecraft - Celephais 1.0 '{Horror}.docx
Howard Philips Lovecraft - Cheia de argint 1.0 '{Horror}.docx
Howard Philips Lovecraft - Chemarea lui Cthulhu 1.0 '{Horror}.docx
Howard Philips Lovecraft - Clericul blestemat 1.0 '{Horror}.docx
Howard Philips Lovecraft - Copacul 1.0 '{Horror}.docx
Howard Philips Lovecraft - Corabia alba 1.0 '{Horror}.docx
Howard Philips Lovecraft - Cosmarul din Innsmouth 1.0 '{Horror}.docx
Howard Philips Lovecraft - Culoarea din afara spatiului 1.0 '{Horror}.docx
Howard Philips Lovecraft - Dagon 1.1 '{Horror}.docx
Howard Philips Lovecraft - Demoni si miracole 1.0 '{Horror}.docx
Howard Philips Lovecraft - Depozitia lui Randolph Carter 1.0 '{Horror}.docx
Howard Philips Lovecraft - Descendentul 1.0 '{Horror}.docx
Howard Philips Lovecraft - Dincolo de poarta cheii de argint 1.0 '{Horror}.docx
Howard Philips Lovecraft - Dincolo de zidul somnului 1.1 '{Horror}.docx
Howard Philips Lovecraft - Dulaul 1.0 '{Horror}.docx
Howard Philips Lovecraft - El 1.0 '{Horror}.docx
Howard Philips Lovecraft - Fapte privitoare la defunctul Arthur Jermyn si familia lui 1.0 '{Horror}.docx
Howard Philips Lovecraft - Fiara din pestera 1.0 '{Horror}.docx
Howard Philips Lovecraft - Herbert West reanimatorul 1.0 '{Horror}.docx
Howard Philips Lovecraft - Hypnos 1.1 '{Horror}.docx
Howard Philips Lovecraft - Ilustratia din carte 1.0 '{Horror}.docx
Howard Philips Lovecraft - In cautarea cetatii Kadath 1.0 '{Horror}.docx
Howard Philips Lovecraft - Intre zidurile din Eryx 1.0 '{Horror}.docx
Howard Philips Lovecraft - Lumea de dincolo 1.0 '{Horror}.docx
Howard Philips Lovecraft - Monstrul din prag 1.0 '{Horror}.docx
Howard Philips Lovecraft - Nyarlathotep 1.0 '{Horror}.docx
Howard Philips Lovecraft - O aparitie pe clar de luna 1.0 '{Horror}.docx
Howard Philips Lovecraft - Oroarea de la Red Hook 1.0 '{Horror}.docx
Howard Philips Lovecraft - Pastorul blestemat 1.0 '{Horror}.docx
Howard Philips Lovecraft - Pisicile din Ulthar 1.0 '{Horror}.docx
Howard Philips Lovecraft - Polaris 1.0 '{Horror}.docx
Howard Philips Lovecraft - Prizonierul faraonilor 1.0 '{Horror}.docx
Howard Philips Lovecraft - Sarbatoare 1.0 '{Horror}.docx
Howard Philips Lovecraft - Sobolanii din ziduri 1.0 '{Horror}.docx
Howard Philips Lovecraft - Strada 1.0 '{Horror}.docx
Howard Philips Lovecraft - Templul 1.0 '{Horror}.docx
Howard Philips Lovecraft - Tranzitia lui Juan Romero 1.0 '{Horror}.docx
Howard Philips Lovecraft - Veneticul 1.0 '{Horror}.docx
Howard Philips Lovecraft - Zeii ceilalti 1.0 '{Horror}.docx

./Howard Roughan:
Howard Roughan - Promisiune mincinoasa 0.9 '{Suspans}.docx

./Howard Taylor:
Howard Taylor - Biografia prescurtata a lui Hudson Taylor 0.8 '{Biografie}.docx

./Hubert Lampo:
Hubert Lampo - Madona din Nedermunster 0.8 '{Literatura}.docx

./Hubert Monteilhet:
Hubert Monteilhet - In capcana 1.0 '{Thriller}.docx

./Hubert Selby Jr.:
Hubert Selby Jr. - Recviem pentru un vis 0.9 '{Necenzurat}.docx

./Hugh Howey:
Hugh Howey - Silozul - V1 Silozul 1.0 '{SF}.docx
Hugh Howey - Silozul - V2 Inceputurile 1.0 '{SF}.docx
Hugh Howey - Silozul - V3 Generatiile 1.0 '{SF}.docx

./Hugo Bettauer:
Hugo Bettauer - Sub spanzuratorare. ucigasul femeilor 1.0 '{Politista}.docx

./Hunter S. Thompson:
Hunter S. Thompson - Spaime si scarbe in Las Vegas 0.6 '{Aventura}.docx

./Hwang Sok:
Hwang Sok - Yong printesa Bari 2.0 '{Diverse}.docx
```

